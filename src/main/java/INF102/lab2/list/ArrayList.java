package INF102.lab2.list;

import java.util.Arrays;

public class ArrayList<T> implements List<T> {

	
	public static final int DEFAULT_CAPACITY = 10;
	
	private int n;
	
	private Object elements[];
	
	public ArrayList() {
		elements = new Object[DEFAULT_CAPACITY];
	}
		
	@Override
	public T get(int index) {
		if (index < 0 || n <= index) {
			throw new IndexOutOfBoundsException();
		}
		return (T) elements[index];
	}
	
	@Override
	public void add(int index, T element) {
		if (n % 10 == 0) {
			Object[] newlist = new Object[n+10];
			for (int i = 0; i < n; i++) {
				newlist[i] = elements[i];
			}
			elements = newlist;
		}
		for (int i = n; index < i; i--) {
			elements[i] = elements[i-1];
		}
		elements[index] = element;
		n++;
	}
	
	@Override
	public int size() {
		return n;
	}

	@Override
	public boolean isEmpty() {
		return n == 0;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder(n*3 + 2);
		str.append("[");
		for (int i = 0; i < n; i++) {
			str.append((T) elements[i]);
			if (i != n-1)
				str.append(", ");
		}
		str.append("]");
		return str.toString();
	}

}